# ecScrape

This repository provides Python scripts to scrape forecast data from the ECMWF.

**The development of ecScrape has moved!**

* http://github.com/mpimet/ecscrape
* http://pypi.org/project/ecscrape
