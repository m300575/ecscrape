#!/bin/bash

module purge
module load py-python-swiftclient
module load python3/unstable
source activate ecscrape

SWIFTURL='swift://swift.dkrz.de/dkrz_948e7d4bbfbb445fbff5315fc433e36a/data_ecmwf'

ecscrape --stream=oper --cache=/work/mh0066/m300575/ecscrape/archive --outdir="${SWIFTURL}"
# ecscrape --time=2024-09-14T12Z --stream=oper --cache=/work/mh0066/m300575/ecscrape/archive --outdir="${SWIFTURL}"
